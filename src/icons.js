import React from 'react';
const icon_height = 25;
const attr_icon_height = 20;

export const ICONS = {
  alcohol: (<img src="http://prts.wiki/images/a/a8/%E9%81%93%E5%85%B7_%E5%B8%A6%E6%A1%86_%E6%89%AD%E8%BD%AC%E9%86%87.png" height={icon_height} />),

  rma: (<img src="http://prts.wiki/images/7/7c/%E9%81%93%E5%85%B7_%E5%B8%A6%E6%A1%86_RMA70-12.png" height={icon_height} />),

  rock: (<img src="http://prts.wiki/images/e/e7/%E9%81%93%E5%85%B7_%E5%B8%A6%E6%A1%86_%E7%A0%94%E7%A3%A8%E7%9F%B3.png" height={icon_height} />),

  d32: (<img src="http://prts.wiki/images/7/76/%E9%81%93%E5%85%B7_%E5%B8%A6%E6%A1%86_D32%E9%92%A2.png" height={icon_height} />),

  mine: (<img src="https://dss1.bdstatic.com/6OF1bjeh1BF3odCf/it/u=1672017263,313384077&fm=74&app=80&f=JPEG&size=f121,121?sec=1880279984&t=807e81fe43f1078031ace9ef194ff68a" height={attr_icon_height} />),

  block: (<img src="http://img5.imgtn.bdimg.com/it/u=1153648212,1889048357&fm=26&gp=0.jpg" height={attr_icon_height} />),

};

export const food_icons = [
  <span style={{color:"#00cd00"}}>■</span>,
  <span style={{color:"#1e90ff"}}>■</span>,
  <span style={{color:"rgb(229,131,8)"}}>■</span>,
  <span style={{color:"red"}}>■</span>,
];